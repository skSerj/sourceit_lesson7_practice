package com.company.model;

public class Magazine extends Literature {
    private String publishingHouse;

    public Magazine(String title, int yearOfPublishing, String publishinghouse) {
        super(title, yearOfPublishing);
        this.publishingHouse = publishinghouse;
    }
    @Override
    public String toString() {
        return String.format("%s; publishingHouse: %s", super.toString(), publishingHouse);
    }
}
