package com.company.model;

public class Literature {
    private String title;
    private int yearOfPublishing;

    public Literature(String title, int yearOfPublishing) {
        this.title = title;
        this.yearOfPublishing = yearOfPublishing;
    }
    @Override
    public String toString() {
        return String.format("name: %s; year: %d", title, yearOfPublishing);
    }
}
