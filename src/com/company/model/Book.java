package com.company.model;

public class Book extends Literature {
    private String autor;

    public Book(String title, int yearOfPublishing, String autor) {
        super(title, yearOfPublishing);
        this.autor = autor;
    }

    @Override
    public String toString() {
        return String.format("%s; author: %s", super.toString(), autor);
    }
}
