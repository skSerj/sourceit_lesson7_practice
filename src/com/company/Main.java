package com.company;

import com.company.model.Book;
import com.company.model.Magazine;

public class Main {

    public static void main(String[] args) {
        Book[] books = new Book[2];
        books[0] = new Book("Медицинская жимия", 1992, "Е.В. Григорьева");
        books[1] = new Book("Технология лекарств", 2010, "Е.В. Иванова");


        Magazine[] magazines = new Magazine[2];
        magazines[0] = new Magazine("АвтоРу",2018,"За рулём");
        magazines[1] = new Magazine("Как устроена Земля",2010,"Наука");

        for (Book book: books) {
            System.out.println(book);
        }

        for (Magazine magazine: magazines) {
            System.out.println(magazine);
        }
    }
}
